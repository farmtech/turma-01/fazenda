import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FazendaController } from './fazenda/fazenda.controller';

@Module({
  imports: [],
  controllers: [AppController, FazendaController],
  providers: [AppService],
})
export class AppModule {}
