import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { Fazenda } from './fazenda';
import { FazendaService } from './fazenda.service';

@Controller('fazenda')
export class FazendaController {

    fazendaService:FazendaService = new FazendaService();

    @Get()
    getFazenda(): Fazenda[]{
        return this.fazendaService.listar();
    }

    @Post()
    saveFazenda(@Body() fazenda:Fazenda ){
        this.fazendaService.save(fazenda);
    }

    @Delete(":id")
    deleteFazenda(@Param("id") id:number){
        this.fazendaService.delete(id);
    }

}
