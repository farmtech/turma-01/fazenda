import { ReadableByteStreamController } from "stream/web";
import { Fazenda } from "./fazenda";

export class FazendaService{

    fazendas:Fazenda[] = [];

    constructor(){
        
    }

    listar():Fazenda[]{
        return this.fazendas;
    }

    save(fazenda: Fazenda){
        fazenda.id = Math.floor(Math.random() * 1000) + 1;
        this.fazendas.push(fazenda);
    }

    delete(id: number){
        let fazendaId = this.fazendas.findIndex(p => p.id == id);
        this.fazendas.splice(fazendaId, 1);
    }

}