import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return "Ola mundo... aqui estamos no FARMTECH ... far far away";
  }

  @Get('/outra')
  getOutraMsg(): string{
    return "Outra msg far far away"
  }
}
